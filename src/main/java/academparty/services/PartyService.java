package academparty.services;

import academparty.models.Person;
import academparty.repositories.PartyRepository;
import academparty.models.Party;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class PartyService {

    private PartyRepository partyRepository;

    @Autowired
    public PartyService(PartyRepository partyRepository) {
        this.partyRepository = partyRepository;
    }

    public Party provideParty(String id) {
        return partyRepository.fetchParty(id);
    }

    public Party updateParty(String id, Party party) {
        return partyRepository.updateParty(id, party);
    }
    public Party addPerson (String id, Person person) {
        Party party = partyRepository.fetchParty(id);
        party.addParticipant(person);
        return party;
    }

    public void deleteParty(String id) {
        partyRepository.deleteParty(id);
    }

    public void deletePerson(String partyId, String personId) {
        partyRepository.deletePerson(partyId, personId);
    }

    public Party createParty(Party party) {
        partyRepository.createParty(party);
        return party;
    }

    public Collection<Party> provideParties(List<String> param) {
        return partyRepository.getParties(param);
    }

}
