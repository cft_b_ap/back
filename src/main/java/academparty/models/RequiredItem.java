package academparty.models;

import javax.validation.constraints.NotNull;

public class RequiredItem {
    @NotNull
    private String name;
    @NotNull
    private Integer requiredAmount;
    private Integer currentAmount;

    public RequiredItem(){
        currentAmount = 0;
    }

    public void setCurrentAmount(Integer currentAmount) {
        this.currentAmount = currentAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRequiredAmount() {
        return requiredAmount;
    }

    public void setRequiredAmount(Integer requiredAmount) {
        this.requiredAmount = requiredAmount;
    }

    public Integer getCurrentAmount() {
        return currentAmount;
    }

    public void increaseCurrentAmount(Integer number) {
        currentAmount+= number;
    }
}
