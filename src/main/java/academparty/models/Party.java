package academparty.models;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Party implements Serializable {
    private String id;
    @NotNull
    private String name;
    @NotNull
    private String host;
    @NotNull
    private String place;
    private Long date;
    private String pictureUrl;
    private String description;
    private Integer maxPersons;
    private List<RequiredItem> requiredItems = new ArrayList<>();
    private List<Person> participants = new ArrayList<>();

    public Party() {
    }

    public Party(String id, String name, String host, String place, Long date, String pictureUrl, String description, Integer maxPersons, List<RequiredItem> requiredItems) {
        this.id = id;
        this.name = name;
        this.host = host;
        this.place = place;
        this.date = date;
        this.pictureUrl = pictureUrl;
        this.description = description;
        this.maxPersons = maxPersons;
        this.requiredItems = requiredItems;
        this.participants = new ArrayList<>();
        this.participants.add(new Person (host));
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Party{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", place='" + place + '\'' +
                ", date=" + date + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", description='" + description + '\'' +
                ", maxPersons=" + maxPersons +
                ", participants=" + participants +
                '}';
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
//        if(!this.participants.contains(host)){
//            this.participants.add(new Person (host));
//        }
        this.host = host;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Integer getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(Integer maxPersons) {
        this.maxPersons = maxPersons;
    }

    public Integer getCurrentPersons() {
        return this.participants.size();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Person> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Person> participants) {
        this.participants = participants;
    }

    public void addParticipant(Person participant) {
        if (participants.size() < maxPersons && !participants.contains(participant)
                && participant.getName()!= null
                && !participant.getName().trim().isEmpty()) {
                participants.add(participant);
            for(Item item : participant.getItems()) {
                for (RequiredItem reqItem : this.getRequiredItems()) {
                    if (reqItem.getName().equals(item.getName())) {
                        reqItem.increaseCurrentAmount(item.getAmount());
                    }
                }
            }
        }
            else throw new IllegalArgumentException();

    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public List<RequiredItem> getRequiredItems() {
        return requiredItems;
    }

    public void addRequiredItem(RequiredItem reqItem) {
        this.requiredItems.add(reqItem);
    }

    public void setRequiredItems(List<RequiredItem> requiredItems) {
        this.requiredItems = requiredItems;
    }
}

