package academparty.database;

import academparty.models.Party;
import academparty.models.Person;
import academparty.models.RequiredItem;
import academparty.models.Item;

import java.io.*;
import java.util.*;

public class Parser {
    private String fileName;

    public Parser() {
        this.fileName = "src/main/java/academparty/database/db.txt";
    }

    public Parser(String fileName) {
        this.fileName = fileName;
    }

    public Collection<Party> execute() throws Exception {

        File file = new File(this.fileName).getCanonicalFile();
        FileReader reader = new FileReader(file);
        Scanner scan = new Scanner(reader);

        Collection<Party> list = new ArrayList<>();

        while (scan.hasNextLine()) {
            String str = scan.nextLine();

            // Comment`s or empty line
            if (str.isEmpty()) {
                continue;
            }
            if ('#' == str.charAt(0)) {
                continue;
            }

            Party party = new Party();

            while (!str.isEmpty()) {
                int index = str.indexOf(' ');
                String field = str.substring(0, index);
                str = str.substring(index + 3);

                switch (field) {
                    case "id":
                        party.setId(str);
                        break;
                    case "name":
                        party.setName(str);
                        break;
                    case "host":
                        party.setHost(str);
                        break;
                    case "place":
                        party.setPlace(str);
                        break;
                    case "date":
                        Long date = (long) Long.parseLong(str);
                        party.setDate(date);
//						party.setDate(str);
                        break;
                    case "pictureUrl":
                        party.setPictureUrl(str);
                        break;
                    case "description":
                        party.setDescription(str);
                        break;
                    case "maxPersons":
                        Integer maxPersons = Integer.parseInt(str);
                        party.setMaxPersons(maxPersons);
                        break;
                    case "count_of_participants":
                        int count = Integer.parseInt(str);
                        for (int i = 0; i < count; ++i) {
                            if (!scan.hasNextLine()) { break; }

                            Person person = new Person(scan.nextLine());
                            String countItemStr = scan.nextLine();
                            index = countItemStr.indexOf(' ');
                            countItemStr = countItemStr.substring(index + 3);

                            int count2 = Integer.parseInt(countItemStr);
                            for(int j = 0; j < count2; ++j){
                                if (!scan.hasNextLine()) { break; }

                                Item item = new Item();
                                item.setName(scan.nextLine());
                                item.setAmount(Integer.parseInt(scan.nextLine()));
                                person.addItem(item);
                            }

                            party.addParticipant(person);
                        }
                        break;
                    case "count_requiredItem":
                        int count3 = Integer.parseInt(str);
                        for(int i = 0; i < count3; ++i){
                            if (!scan.hasNextLine()) { break; }

                            RequiredItem requiredItem = new RequiredItem();
                            requiredItem.setName(scan.nextLine());
                            requiredItem.setRequiredAmount(Integer.parseInt(scan.nextLine()));
                            requiredItem.setCurrentAmount(Integer.parseInt(scan.nextLine()));
                            party.addRequiredItem(requiredItem);
                        }

                    default:
                        //??
                }
                if (!scan.hasNextLine()) {
                    break;
                }
                str = scan.nextLine();
            }

            list.add(party);
        }
        reader.close();
        return list;
    }

    public void addToDataBase(Writer writer, Party party) throws IOException {

        writer.append('\n');
        String value = party.getId();
        if (null != value) {
            writer.write("id : " + value + '\n');
        }

        value = party.getName();
        if (null != value) {
            writer.write("name : " + value + '\n');
        }

        String host = party.getHost();
        if (null != host) {
            writer.write("host : " + host + '\n');
        }

        value = party.getPlace();
        if (null != value) {
            writer.write("place : " + value + '\n');
        }

        Long date = party.getDate();
        if (null != date) {
            value = Long.toString(date);
            writer.write("date : " + value + '\n');
        }

        value = party.getPictureUrl();
        if (null != value) {
            writer.write("pictureUrl : " + value + '\n');
        }

        value = party.getDescription();
        if (null != value) {
            writer.write("description : " + value + '\n');
        }

        Integer maxPersons = party.getMaxPersons();
        if (null != maxPersons) {
            value = Integer.toString(maxPersons);
            writer.write("maxPersons : " + value + '\n');
        }

        List<RequiredItem> listRequiredItems = party.getRequiredItems();
        if (null != listRequiredItems) {
            value = Integer.toString(listRequiredItems.size());
            writer.write("count_requiredItem : " + value + '\n');
            for (int i = 0; i < listRequiredItems.size(); ++i) {
                value = listRequiredItems.get(i).getName();
                value += '\n';
                value += Integer.toString(listRequiredItems.get(i).getRequiredAmount());
                value += '\n';
//                value += Integer.toString(listRequiredItems.get(i).getCurrentAmount());
                value += '0';
                writer.write(value + '\n');
            }
        }

        List<Person> list = party.getParticipants();
        if (null != list) {
            if (0 < list.size()) {
                value = Integer.toString(list.size());
                writer.write("count_of_participants : " + value + '\n');
                for (int i = 0; i < list.size(); ++i) {
                    Person person = list.get(i);
                    value = person.getName();
                    value += '\n';
                    value += "count_Item : ";
                    value += Integer.toString(person.getItems().size());
                    for(int j = 0; j < person.getItems().size(); ++j) {
                        value += '\n';
                        value += person.getItems().get(j).getName();
                        value += '\n';
                        value += Integer.toString(person.getItems().get(j).getAmount());
                        value += '\n';
                    }
                    writer.write(value);
                }
            }
        }
        writer.write('\n');
        writer.flush();
    }

    public void addToBaseData(Collection<Party> collection) throws IOException {
        File file = new File(this.fileName).getCanonicalFile();
        FileReader reader = new FileReader(file);
        Scanner scan = new Scanner(reader);

        String comments = "";
        while (scan.hasNextLine()) {
            String str = scan.nextLine();
            if (str.isEmpty()) {
                break;
            }
            if ('#' == str.charAt(0)) {
                comments = comments + str + '\n';
            } else {
                break;
            }
        }
        comments += "\n";
        reader.close();

        FileWriter writer = new FileWriter(file, false);
        writer.write(comments);
        writer.flush();

        List<Party> list = new ArrayList<>(collection);
        for (Party party : list) {
            addToDataBase(writer, party);
        }
        writer.close();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}


