package academparty.api;

import java.io.Serializable;

public class ErrorMessage implements Serializable {
    private final String message;

    public ErrorMessage(String msg) {
        message = msg;
    }

    public String getMessage() {
        return message;
    }
}
