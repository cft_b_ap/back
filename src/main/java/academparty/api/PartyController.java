package academparty.api;


import academparty.models.Person;
import academparty.services.PartyService;
import academparty.models.Party;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.beans.Expression;
import java.util.*;

@RestController
public class PartyController {
    private static final String PARTIES_PATH = "/api/v001/parties";

    @Autowired
    private PartyService service;

    @GetMapping(PARTIES_PATH + "/{id}")
    public ResponseEntity<?> readParty(@PathVariable String id) {
        try {
            Party party = service.provideParty(id);
            return ResponseEntity.ok(party);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage("Мероприятие не найдено"));
        }
    }

//    @GetMapping(PARTIES_PATH + "/exit")
//    public void exit() {
//        System.exit(10);
//    }
    @PostMapping(PARTIES_PATH)
    public ResponseEntity<?> createParty(@RequestBody @Valid Party party) {
        try {
        Party result = service.createParty(party);
        return ResponseEntity.ok(result);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage("Какое-то необходимое поле - пустое."));
        }
    }

    @DeleteMapping(PARTIES_PATH + "/{id}")
    public ResponseEntity<?> deleteParty(@PathVariable String id) {
        try {
            service.deleteParty(id);
            return ResponseEntity.ok(new OkMessage());
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage("Мероприятие не найдено"));
        }
    }

    @PatchMapping(PARTIES_PATH + "/deleteperson/{partyId}")
    public ResponseEntity<?> deletePerson(@PathVariable String partyId, @RequestBody Person person) {
        try {
            service.deletePerson(partyId, person.getName());
            return ResponseEntity.ok(service.provideParty(partyId));
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage("Мероприятие или человек не найдены"));
        }
    }

    @PatchMapping(PARTIES_PATH + "/{id}")
    public ResponseEntity<?> updateParty(@PathVariable String id, @RequestBody Party party) {
        try {
            Party result = service.updateParty(id, party);
            return ResponseEntity.ok(result);
        } catch (IndexOutOfBoundsException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage("Мероприятие не найдено или в теле указан не тот ID."));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage("Нельзя поставить максимальное количество участников меньше, чем сейчас уже участвует."));

        }
    }

//    @PatchMapping(PARTIES_PATH)
//    public ResponseEntity<?> check() {
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage("Тусовка не найдена"));
//    }

    @PatchMapping(PARTIES_PATH + "/addperson/{id}")
    public ResponseEntity<?> addPerson(@PathVariable String id, @RequestBody Person person) {
        try {
            Party result = service.addPerson(id, person);
            return ResponseEntity.ok(result);
        } catch (IndexOutOfBoundsException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage("Мероприятие не найдено"));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage("Такой человек уже участвует или превышен лимит."));
        }
    }


    // Checking sender-name with host-name
    // The list of methods that should contain this checking :
    //      updateParty(); deletePerson(); deleteParty();
    // Example:
//    @PatchMapping(PARTIES_PATH + "/check_host_name/{partyId}/{userId}")
//    public ResponseEntity<Party> updatePartyWithCheck(@PathVariable String partyId, @PathVariable String userId, @RequestBody Party party) {
//        try {
//            checkHostName(userId);
//            Party result = service.updateParty(partyId, party);
//            return ResponseEntity.ok(result);
//        } catch (Exception e) {
//            return ResponseEntity.badRequest().build();
//        }
//    }
//
//    private boolean checkHostName(String userName) throws RuntimeException {
//        Party party = service.provideParty(userName);
//        if(!userName.equals(party.getHost())){
//            throw new RuntimeException("Violated the access level of the user");
//        }
//        return true;
//    }

    // Sorting with a parameter
    // Possible case:
    //      presence of the sender; from date to date; place; count of persons
    // Example:
    // Get requests:
    //      ../api/v001/parties?param=participant&param=Кирилл
    //      ../api/v001/parties?param=date
    //      ../api/v001/parties?param=place&param=NSU
    //      ../api/v001/parties?param=host&param=Кирилл
    //      ../api/v001/parties?param=currentpersons
    //      ../api/v001/parties?param=currentpersons&param=less
    //      ../api/v001/parties?param=currentpersons&param=more&param=10
    @GetMapping(PARTIES_PATH)
        public ResponseEntity<?> listParties(@RequestParam(required = false) List<String> param){
        Collection<Party> parties = service.provideParties(param);
        if(null == parties){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage("Мероприятий не найдено."));
        }
        return ResponseEntity.ok(parties);
    }
}
