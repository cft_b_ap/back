package academparty.repositories;

import academparty.database.Parser;
import academparty.models.Item;
import academparty.models.Party;
import academparty.models.Person;
import academparty.models.RequiredItem;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;
import java.util.*;

import static java.lang.System.currentTimeMillis;


@Repository
public class InMemoryPartyRepository implements PartyRepository {

    private Map<String, Party> partyCache = new HashMap<>();

    public InMemoryPartyRepository() {
//        ShutdownHook shutdownHook = new ShutdownHook(this);
//        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }

    @PostConstruct
    public void DownLoadData() {
        Parser parser = new Parser();
        try {
            Collection<Party> collection = parser.execute();
            for(Party party : collection) {
                partyCache.put(party.getId(), party);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Party fetchParty(String id) {
        if (!partyCache.containsKey(id))
           throw new IndexOutOfBoundsException();
        return partyCache.get(id);
    }

    @Override
    public Party updateParty(String id, Party party) {
        if (!id.equals(party.getId()))
            throw new IndexOutOfBoundsException();
        //partyCache.put(id, party);
        Party cachedParty = partyCache.get(id);
        if (party.getDate()!= null)
            cachedParty.setDate(party.getDate());
        if (party.getHost() != null && !party.getHost().trim().isEmpty()) {
            cachedParty.setHost(party.getHost());
        }
        if (party.getPlace() != null && !party.getPlace().trim().isEmpty()) {
            cachedParty.setPlace(party.getPlace());
        }
        if (party.getDescription()!= null && !party.getDescription().trim().isEmpty())
            cachedParty.setDescription(party.getDescription());
        if (party.getPictureUrl()!= null && !party.getPictureUrl().trim().isEmpty())
            cachedParty.setPictureUrl(party.getPictureUrl());
        if (party.getName() != null && !party.getName().trim().isEmpty()) {
            cachedParty.setName(party.getName());
        }
        if (party.getMaxPersons()!= null) {
            if (party.getMaxPersons() >= cachedParty.getCurrentPersons())
                cachedParty.setMaxPersons(party.getMaxPersons());
            else
                throw new IllegalArgumentException();
        }
        if (party.getRequiredItems() != null) {
            cachedParty.setRequiredItems(party.getRequiredItems());
        }
        return cachedParty;
    }

    @Override
    public void deleteParty(String id) {
        if (!partyCache.containsKey(id))
            throw new IllegalArgumentException();
        partyCache.remove(id);
    }

    @Override
    public void deletePerson(String partyId, String personId) {
        if (!partyCache.containsKey(partyId))
            throw new IndexOutOfBoundsException();
        //partyCache.get(partyId).getParticipants()
        for (Person per : partyCache.get(partyId).getParticipants()) {
            if (per.getName().equals(personId)) {
                if (!per.getItems().isEmpty()) {
                    for (Item it : per.getItems()) {
                        for (RequiredItem ri : partyCache.get(partyId).getRequiredItems()) {
                            if (ri.getName().equals(it.getName())) {
                                ri.setCurrentAmount(ri.getCurrentAmount() - it.getAmount());
                                break;
                            }
                        }
                    }
                }
                partyCache.get(partyId).getParticipants().remove(per);
                break;
            }
        }
    }

    @Override
    public Party createParty(Party party) {
        if (party.getName().trim().isEmpty()
                || party.getHost().trim().isEmpty()
                || party.getPlace().trim().isEmpty()
                || party.getMaxPersons() == null
                || party.getDate() == null){
            throw new IllegalArgumentException();
        }
        party.setId(UUID.randomUUID().toString());
        party.addParticipant(new Person (party.getHost()));

        if (party.getPictureUrl() == null || party.getPictureUrl().trim().isEmpty()) {
            party.setPictureUrl("https://vk.com/images/camera_200.png");
        }

        partyCache.put(party.getId(), party);
        return party;
    }

    @Override
    public Collection<Party> getParties(List<String> param) {
        if(null == param) {
            return partyCache.values();
        }
        List<Party> parties = new ArrayList<>(partyCache.values());

        if(0 >= param.size()) {return null;}
        switch(param.get(0)){
            case "participant":
                if(1 >= param.size()) {return null;}
                String value = param.get(1);

                for (int i = 0; i < parties.size(); ++i){
                    boolean del = true;
                    for(Person person : parties.get(i).getParticipants()){
                        if(value.equals(person.getName())){
                            del = false;
                            break;
                        }
                    }
                    if(del){
                        parties.remove(i);
                        --i;
                    }
                }

                break;

            case "date":
                if(1 == param.size()) {
                    Collections.sort(parties, (o1, o2) -> (int) (o2.getDate() - o1.getDate()));
                    Collections.sort(parties, (o1, o2) -> {
                        if(o2.getDate() < currentTimeMillis() || o1.getDate() < currentTimeMillis()) { return 0;}
                        return (int)(o1.getDate() - o2.getDate());
                    });
                }else if(2 == param.size()){
                    switch (param.get(1)){
                        case "more":
                            Collections.sort(parties, (o1, o2) -> (int)(o2.getDate  () - o1.getDate()));
                            break;
                        case "less":
                            Collections.sort(parties, (o1, o2) -> (int)(o1.getDate() - o2.getDate()));
                            break;
                        default:
                            return null;
                    }
                }else if(3 == param.size()){
                    Long dateFrom = Long.parseLong(param.get(1));
                    Long dateTo = Long.parseLong(param.get(2));
                    Collections.sort(parties, (o1, o2) -> (int) (o1.getDate() - o2.getDate()));
                    for(int i = 0; i < parties.size(); ++i){
                        Long date = parties.get(i).getDate();
                        if(dateFrom > date || date > dateTo){
                            parties.remove(i);
                            --i;
                        }
                    }
                }
                break;

            case "place":
                if(1 >= param.size()) {return null;}
                value = param.get(1);

                for (int i = 0; i < parties.size(); ++i){
                    if(!value.equals(parties.get(i).getPlace())){
                        parties.remove(i);
                        --i;
                    }
                }
                break;

            case "host":
                if(1 >= param.size()) {return null;}
                value = param.get(1);
                for (int i = 0; i < parties.size(); ++i){
                    if(!value.equals(parties.get(i).getHost())){
                        parties.remove(i);
                        --i;
                    }
                }
                break;

            case "currentpersons":
                if(1 == param.size()) {
                    param.add(1,"more");
                }
                switch (param.get(1)){
                    case "more":
                        Collections.sort(parties, (o1, o2) -> (o2.getParticipants().size() - o1.getParticipants().size()));
                        if(2 == param.size()) {break;}
                        Integer cmp = Integer.parseInt(param.get(2));
                        for(int i = parties.size()-1; i >= 0; --i) {
                            if(cmp >= parties.get(i).getParticipants().size()) {
                                parties.remove(i);
                            }else {
                                break;
                            }
                        }
                        break;
                    case "less":
                        Collections.sort(parties, (o1, o2) -> (o1.getParticipants().size() - o2.getParticipants().size()));
                        if(2 == param.size()) {break;}
                        cmp = Integer.parseInt(param.get(2));
                        for(int i = parties.size()-1; i >= 0; --i) {
                            if (cmp <= parties.get(i).getParticipants().size()) {
                                parties.remove(i);
                            } else {
                                break;
                            }
                        }
                        break;
                    default:
                        return null;
                }
                break;

            default:
                return null;
        }

        return parties;
    }

    @Override
    public void saveData(){
        Parser parser = new Parser();
        try{
            parser.addToBaseData(partyCache.values());
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    @PreDestroy
    public void delete(){
        this.saveData();
    }
}