package academparty.repositories;

import academparty.models.Party;
import academparty.models.Person;

import java.util.Collection;
import java.util.List;

/**
 * Интерфейс для получения данных по party
 */
public interface PartyRepository {

    Party fetchParty(String id);

    Party updateParty(String id, Party party);

    void deleteParty(String id);

    void deletePerson (String partyId, String personId);

    Party createParty(Party party);

    Collection<Party> getParties(List<String> param);

    void saveData();
}
